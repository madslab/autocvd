AutoCVD is a design of a chemical vapour deposition (CVD) furnace for the 
growth of 2D materials. It consists of design of hardware and a controller
software for managing that hardware. 

AutoCVD has following features:

- Modular hardware and software design
- Can be completely automated by scripting
- Provides access to large span of CVD parameters
- Core of the software can be used as controller for various other hardware
  setups
- HTML5/Webcomponents based user interface that accesses the controller
  transparently over the network

The design has been running reliably at two sites for total of ~5 years.
The core software has been used as base of controller for three different 
systems.

This repository provides all the software that is used to run the CVD setup
as well as some documentation on the hardware. The detailed design will be
described elsewhere.


