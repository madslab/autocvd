def funcmap(devname, devid):
    m = n112(devname, devid)
    c = {'init_state': m.init_state, 'get_flow': m.get_flow, 'set_flow': m.set_flow,
            'get_fs_range': m.get_fs_range, 'handle': m}
    return c, m.errcodes

#_TEST = False
_DEBUG = False # turn this on for debug output on console.

import serial 
import threading
_baud_rates = [38400]
_tm_o = {38400: 0.1, 9600: 0.1}
# we are working on Linux/Rpi. Otherwise use 'COM?' for portname.
chan_d = {'lock':threading.RLock(), 'portname':'/dev/mfcs', 'port': None, 'cbr': 38400, 'timeout':_tm_o[38400], 'error_attempts':9}
# DMFC only handle upto 9 error responses.
def port_open(port=chan_d['portname'], br=chan_d['cbr']): 
    return serial.Serial(port, baudrate=br, parity=serial.PARITY_ODD, timeout=chan_d['timeout'], bytesize=serial.SEVENBITS)
# 1 start bit, 1 stop bit, 7 char bits, odd parity.
def set_port(p):
    global chan_d
    chan_d['port'] = p
def port_close(): chan_d['port'].close()
def init_comm():
    set_port(port_open())
    
_start, _pad, _stx, _etx, _enq, _ack, _lf, _cr, _nak, _star, _at = \
    b'@', b'\x00', b'\x02', b'\x03', b'\x05', b'\x06', b'\x0A', b'\x0D', b'\x15', b'\x2A', b'\x40'
    #spl_chars = {'STX':'\x02', 'ETX':'\03', 'ENQ':'\x05', 'ACK':'\06', 'LF':'\x0A', 'CR':'\x0D', 'NAK':'\x15'}
    #_STX, _ETX, _ENQ, _ACK, _LF, _CR, _NAK = '\x02', '\x03', '\x05', '\x06', '\x0A', '\x0D', '\x15'
_mps = 9 # minimum packet size
# note that device is appending \x00 after macid, so _mps is 10 instead of 9.
_ack_pkt_sz = 1

def bcc(x): return bytes([sum(x+_etx)%128])
def chk_resp_bcc(resp): return int.from_bytes(bcc(resp[1:-2]), byteorder='little') == resp[-1]

def form_cmd(func, data, macid):
    b = bcc(func+data)
    if b=='@': b += 3*_etx
    if b=='*': b += 9*_etx
    msg = _start + macid + _stx + func + data + _etx + b
    return msg

# wrapper for sending cmds and recving response
def cmd_io(cmdstr, attempts=chan_d['error_attempts']):
    _PORT = chan_d['port']
    _LOCK = chan_d['lock']
    if attempts==0: raise MFC_Error('MFC no response in 9 attempts')
    with _LOCK:
        _PORT.write(cmdstr)
        resp = b''
        while True:
            c = _PORT.read(1)
            #print(b'|'+c+b'|')
            if c==_nak: # incorrect bcc
                return cmd_io(cmdstr, attempts=attempts-1) 
            if c==_etx: 
                resp += _etx + _PORT.read(1) # read bcc
                break
            else: resp += c
            if c=='' or c==None: break
        #sleep(0.01) # 10 ms gap, as described in manual. omitted.
        ok, res = chk_resp_bcc(resp), resp[1:-2]
        if not ok: 
            _PORT.write(_nak)
            return cmd_io(cmdstr, attempts=attempts-1)
        _PORT.write(_ack)
        return res.decode("utf-8")

class n112():
# fullscale value must have a decimal
    name, macid = None, b'00'
    fs_range, curr_setp, actv_flow, ramptime = None, 0.0, 0.0, 0.0
    errcodes = {-1: 'Reuqested flow is beyond range'}
    unit_scale = {'A':0.91, 'B':1, 'C':1, 'D':1000} # to translate to sccm units 
    def __init__(self, name, macid):
        self.name, self.macid = name, macid
    def init_state(self, args):
        self.fs_range, self.curr_setp = args['fs_range'], args['init_val']
        #set_cbr(self.macid, br=chan_d['cbr'])
        cmd = form_cmd(b'\'!+REVERSE', b'', self.macid)
        resp = cmd_io(cmd)
        #cmd = form_cmd(b'IAC', b'', self.macid)
        #resp = cmd_io(cmd)
        self.set_flow([self.curr_setp])
        self.get_flow([])
        
    def get_fs_range(self, args):
        cmd = form_cmd(b'RFS', b'', self.macid)
        resp = cmd_io(cmd)
        f, unit = resp.split(',')
        return 0, [float(f)*self.unit_scale[unit]]
    def get_flow(self, args):
        cmd = form_cmd(b'RFC', b'', self.macid)
        resp = cmd_io(cmd)
        #print(resp)
        try: return 0, [float(resp)]
        except: return 0, [0.0]
    def set_flow(self, args):
        flowrate = str(args[0])+',B'
        cmd = form_cmd(b'AFC', bytes(flowrate, "utf-8"), self.macid)
        resp = cmd_io(cmd)
        return 0, [args[0]]
    def get_mfgno(self, args):
        cmd = form_cmd(b'#)*', b'', self.macid)
        resp = cmd_io(cmd)
        return 0, [resp]    
    def get_addr(self, args):
        cmd = form_cmd(b'*)&', b'', self.macid)
        resp = cmd_io(cmd)
        return 0, [resp]
    def set_addr(self, args):
        cmd = form_cmd(b'\'*$', b'', self.macid)
        resp = cmd_io(cmd)
        return resp
    def get_modelname(self, args):
        cmd = form_cmd(b'RMN', b'', self.macid)
        resp = cmd_io(cmd)
        return resp
    def get_revision(self, args):
        cmd = form_cmd(b'RRV', b'', self.macid)
        resp = cmd_io(cmd)
        return resp
    def get_version(self, args):
        cmd = form_cmd(b'RVR', b'', self.macid)
        resp = cmd_io(cmd)
        return resp
    def set_valve_state(self, args): #open 'O', close 'C'
        cmd = form_cmd(b'MV', state, self.macid)
        resp = cmd_io(cmd)
        return resp
    def get_state(self, args):
        cmd = form_cmd(b'ROC', b'', self.macid)
        resp = cmd_io(cmd)
        return resp
    def set_slowstart(self, args):
        if target: 
            data = str(target)
            if sweeptime: data + ',' + str(sweeptime)
        cmd = form_cmd('ASS', '', self.macid)
        resp, units = parse_resp(cmd_io(cmd))
        return resp


class MFC_Error(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)



