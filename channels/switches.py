def funcmap(devname, devid):
    if devid[0]=='DIO':
        sw = Switch(devname, devid[1])
        c = {'init_state': sw.init_state, 'get_state': sw.get_state, 'set_state': sw.set_state, 
                'handle': sw }
#                'set_def_state': sw.set_def_state, 'handle': sw }
        return c, sw.errcodes

    
#import threading
#chan_d = {'lock': threading.RLock(), 'port': None}
## RPIO not used anymore since it gives SystemError: RPIO should run on raspberry Pi.
#import RPIO as GPIO 
from gpiozero import LED
_pin_map = {i:i for i in range(2, 17)}
_pin_map[1] = 25

def init_comm(): pass

#when using RPIO:
#def init_comm():
#    global _pin_map
#    GPIO.setmode(GPIO.BCM)
#    GPIO.setwarnings(True)
#    #enable them as outputs
#    for p in _pins_map.values(): GPIO.setup(p, GPIO.OUT)
#    # they can now be used as GPIO.output(p, GPIO.LOW) etc.

from bidict import bidict
class Switch():
    #name, num = None, None #RPIO
    name, sw = None, None
    def_st, curr_st = 0, None
    _onoff = bidict({'noflow':0, 'flow':1})
    errcodes = {-1: 'Invalid requested state',}

    def __init__(self, name, sw_num):
        self.name = name
        #self.num = _pin_map(sw_num)
        self.sw = LED(_pin_map[sw_num], active_high=False)
    def init_state(self, args): #def_st, curr_st):
        if args['default'] == 'flow': 
            self.def_st = 1
            #self._onoff = bidict({'flow':0, 'noflow':1})
            #self._onoff = bidict({'flow':False, 'noflow':True})
        c = self._onoff[args['init_val']]
        #if c != self.def_st:
        self.set_state([c])
        self.curr_st = c
        return 0, [self.curr_st]
    
    def get_state(self, args):
        #r = GPIO.input(self.num)
        self.curr_st = 0
        if self.sw.is_active: self.curr_st = 1
        return 0, [self.curr_st]

    def set_state(self, args):
        # also accepts 0 for noflow and 1 for flow.
        st = args[0]
        if type(st) != int: st = self._onoff[st]
        #r = GPIO.output(self.num, st) #self._onoff[st])
        #r = self._onoff[st]
        if st: self.sw.on()
        else: self.sw.off()
        self.curr_st = 0
        if self.sw.is_active: self.curr_st = 1
        return 0, [self.curr_st]

    #def set_def_state(self, args):
    #    r = GPIO.output(self.num, self._onoff[self.def_st])
    #    return 0, [self.def_st]



