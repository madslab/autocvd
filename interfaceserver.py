#!/usr/bin/env python

"""Websocket server for the Instrument Controller.
Todo: Auth, Error handling, Uniform notation.
"""

import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid

import controlserver

from tornado.options import define, options

import socket
localaddr = socket.gethostbyname(socket.gethostname())
define("ws_ipaddr", default=localaddr, help="Use this ip address for websocket")
define("port", default=8888, help="run on the given port", type=int)

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/controllersocket", ControllerSocketHandler),
        ]
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html", ws_ipaddr='localhost', ws_port=options.port) #, messages=ControllerSocketHandler.cache)
        #self.render("index.html", ws_ipaddr=options.ws_ipaddr, ws_port=options.port) #, messages=ControllerSocketHandler.cache)

class ControllerSocketHandler(tornado.websocket.WebSocketHandler):
    # waiters corresponds to the GUI frontends and are updated simultaneously.
    # todo: only localhost can issue commands, authorisation.
    waiters = set()
    client_ips = list()

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def open(self):
        logging.info("Opened connection from %s", self.request.remote_ip)
        ControllerSocketHandler.waiters.add(self)
        ControllerSocketHandler.client_ips.append(self.request.remote_ip)
        ipnumbers = ' '.join([str(x) for x in self.client_ips])
        logging.info("Now connected to %d clients (%s)", len(self.waiters), ipnumbers)

    def on_close(self):
        ControllerSocketHandler.waiters.remove(self)
        logging.info("Closed connection from ", self.request.remote_ip)

    #@classmethod
    #def update_cache(cls, chat):
    #    cls.cache.append(chat)
    #    if len(cls.cache) > cls.cache_size:
    #        cls.cache = cls.cache[-cls.cache_size:]

    @classmethod
    def send_results(cls, results):
        for waiter in cls.waiters:
            try:
                waiter.write_message(results)
            except:
                logging.error("Error sending message", exc_info=True)

    def on_message(self, message): 
        logging.info("> %r", message)
        parsed = tornado.escape.json_decode(message)
        print(parsed)
        results = controlserver.execute_commands(parsed, client_info={'ip':self.request.remote_ip})
        jsonres = tornado.escape.json_encode(results)
        logging.info("< %r", jsonres)
        ControllerSocketHandler.send_results(jsonres)


def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    controlserver.SERVER_IPADDR = options.ws_ipaddr
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
